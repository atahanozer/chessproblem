import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Piece {
    
	static final char KING = 'K' ;
	static final char QUEEN = 'Q' ;
	static final char BISHOP = 'B' ;
	static final char ROOK = 'R' ;
	static final char KNIGHT = 'N' ;
	static final char THREATENED = '-' ;

    public char name;

    Piece(char name) {
        this.name = name;
    }

    public Set<Position> possiblePositions(Position current, int dimension,ChessBoard board) {
        Set<Position> positions = new HashSet<Position>();

        if (this.name == KING) {
            for (int x = -1; x <= 1; x++) {
                for (int y = -1; y <= 1; y++) {
                    if ((x != 0 || y != 0) && (current.getX()+x > -1 && current.getX()+x < dimension) && (current.getY()+y > -1 && current.getY()+y < dimension)) {
                    	Position position = new Position(current, x, y);
                    	 if (!board.isEmptyOrThreatened(position)) {
                             return null;
                         }
                        positions.add(position);
                    }
                }
            }
        } else if (this.name == QUEEN) {
        	Set<Position> bishopMoves = null;
        	Set<Position> rookMoves = getRookMoves(positions,dimension,current,board);
        	if(rookMoves != null){
        		 bishopMoves = getBishopMoves(positions,dimension,current,board);
        	}
        	if(rookMoves != null && bishopMoves != null){
	            positions.addAll(rookMoves);
	            positions.addAll(bishopMoves);
        	}
        } else if (this.name == ROOK) {
        	Set<Position> rookMovesR = getRookMoves(positions,dimension,current,board);
        	if(rookMovesR == null)
        		return null;
        	positions.addAll(rookMovesR);
        } else if (this.name == BISHOP) {
        	Set<Position> bishopMoves = getBishopMoves(positions,dimension,current,board);
        	if(bishopMoves == null)
        		return null;
        	positions.addAll(bishopMoves);
        } else if (this.name == KNIGHT) {
            for (int x = -2; x <= 2; x++) {
                for (int y = -2; y <= 2; y++) {
                    int absX = Math.abs(x);
                    int absY = Math.abs(y);
                    if ((absX != absY && absX != 0 && absY != 0) && (current.getX()+x > -1 && current.getX()+x < dimension) && (current.getY()+y > -1 && current.getY()+y < dimension) ) {
                    	Position position = new Position(current, x, y);
                    	if (!board.isEmptyOrThreatened(position)) {
	                         return null;
	                     }
                    	positions.add(position);
                    }
                }
            }
        }
        return positions;
    }
    
    public Set<Position> getBishopMoves(Set<Position> positions, int dimension,Position current,ChessBoard board){
    	   int half = (dimension % 2 == 0) ? (dimension / 2) : ((dimension + 1) / 2);
           for (int x = -half; x <= half; x++) {
               for (int y = -half; y <= half; y++) {
                   if ((x != 0 && y != 0 && Math.abs(x) == Math.abs(y)) && (current.getX()+x > -1 && current.getX()+x < dimension) && (current.getY()+y > -1 && current.getY()+y < dimension)) {
	                   	Position position = new Position(current, x, y);
	                   	if (!board.isEmptyOrThreatened(position)) {
		                         return null;
		                     }
                	   positions.add(position);
                   }
               }
           }
       return positions;
    }
    
    public Set<Position> getRookMoves(Set<Position> positions, int dimension,Position current,ChessBoard board){
       
    	for (int i = 0; i < dimension; i++) {
            Position positionX = new Position(i, current.getY());
            Position positionY = new Position(current.getX(), i);

            if (!current.equals(positionX)) {
            	if(!board.isEmptyOrThreatened(positionX))
            		return null;
                positions.add(positionX);
            }
            if (!current.equals(positionY)) {
            	if(!board.isEmptyOrThreatened(positionY))
            		return null;
                positions.add(positionY);
            }
        }
    return positions;
 }
    
    static List<Piece> addPiece(Piece piece, int number) {
        if (number == 0) {
            return Collections.emptyList();
        }

        List<Piece> pieces = new ArrayList<Piece>(number);
        for (int i = 0; i < number; i++) {
            pieces.add(piece);
        }

        return pieces;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Piece cp = (Piece) o;
        if(cp.name == this.name)
        	return true;
        else
        	return false;
        
    }

}