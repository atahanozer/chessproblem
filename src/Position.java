
public final class Position {
    private final int x;
    private final int y;

    public Position(final Position pos, final int x, final int y) {
        this(pos.x + x, pos.y + y);
    }

    public Position(final int x, final int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public boolean isValid(int dimension) {
        return x >= 0 && x < dimension && y >= 0 && y < dimension;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Position position = (Position) o;

        return x == position.x && y == position.y;

    }
}