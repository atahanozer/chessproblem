import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.Deque;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Scanner;
import java.util.Set;

public class Main {

    public static void main(String args[]) {
		int kings = 0,queens = 0,bishops = 0,rooks = 0,knights = 0,m=0;
    	try {
			FileInputStream fstream = new FileInputStream("input.txt");
			DataInputStream in = new DataInputStream(fstream);
			Scanner sc = new Scanner(new File("input.txt"));

			 m = sc.nextInt();
			
			 kings = sc.nextInt();
			 queens = sc.nextInt();
			 bishops = sc.nextInt();
			 rooks = sc.nextInt();
			 knights = sc.nextInt();

			in.close();
			sc.close();
		} catch (Exception e) {
			System.out.println("*Input error: Check input format. (Sample format: 4 0 0 0 2 4)");
		}

    	Deque<Piece> list = new LinkedList<Piece>();
    	list.addAll(Piece.addPiece(new Piece(Piece.QUEEN),queens));
    	list.addAll(Piece.addPiece(new Piece(Piece.BISHOP),bishops));
    	list.addAll(Piece.addPiece(new Piece(Piece.KNIGHT),knights));
    	list.addAll(Piece.addPiece(new Piece(Piece.KING),kings));
    	list.addAll(Piece.addPiece(new Piece(Piece.ROOK),rooks));
        resolve(list, m, queens+kings+bishops+rooks+knights);
    }

    private static void resolve(Deque<Piece> pieces, int dimension, int numberOfChessPieces) {
        Set<ChessBoard> result = new HashSet<ChessBoard>();

        long startTime = System.currentTimeMillis();
        Piece firstChessPiece = pieces.pollFirst();
        for (int x = 0; x < dimension; x++) {
            for (int y = 0; y < dimension; y++) {
                ChessBoard board = new ChessBoard(dimension, numberOfChessPieces);
                boolean isAdded = board.addChessPiece(firstChessPiece, new Position(x, y));
                if (isAdded) {
                    result.add(board);
                }
            }
        }
        for (Piece piece : pieces) {
            Set<ChessBoard> temp = new HashSet<ChessBoard>(result);
            result.clear();
            for (ChessBoard board : temp) {
       		 if(board.availablePositions.size() < board.pieces)
    			 continue;
            	 for (int x = 0; x < board.availablePositions.size(); x++) {
                         ChessBoard inner = board.copy();
                         boolean isAdded = inner.addChessPiece(piece, board.availablePositions.get(x));
                         if (isAdded) {
                             result.add(inner);
                         }
 
                 }
            }
        }
        long finshTime = System.currentTimeMillis();
		System.out.println("The time spent for 7*7 (King=2, Queen=2, Bishop=2, Knight=1, Rook 0): 28 mins");
        System.out.println("# of combinations: 3063828");
        System.out.println();
        long counter = 0;
        Set<ChessBoard> printed = new HashSet<ChessBoard>();
        for (ChessBoard board : result) {
            if (board.isValid(result) && board.isComplete(printed)) {
                counter++;
                if(counter < 10)
                	System.out.println(board);
                printed.add(board);
            }
        }
        System.out.println("The time spent: " + (finshTime - startTime) + " miliseconds");
        System.out.println("# of combinations (Max 10 of them are printed): " + counter);
    }
}