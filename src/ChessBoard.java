import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

public class ChessBoard {

    private final int dimension;
    private final Piece[][] cells;
    List<Position> availablePositions;
    public int pieces;

    public ChessBoard(int dimension, int pieces) {
        this.dimension = dimension;
        this.cells = new Piece[dimension][dimension];
        this.pieces = pieces;
        
        availablePositions =  new ArrayList<Position>();
        
        for (int x = 0; x < dimension; x++) {
            for (int y = 0; y < dimension; y++) {
            	availablePositions.add(new Position(x,y));
            }
         }
    }

    public ChessBoard(ChessBoard board) {
        this.dimension = board.dimension;
        this.cells = deepCopy(board.cells);
        this.pieces = board.pieces;
        
        availablePositions = new ArrayList<Position>(board.availablePositions);
       /* for (int x = 0; x < board.availablePositions.size(); x++) {
            	availablePositions.add(board.availablePositions.get(x));
         }
        */
    }

    public boolean addChessPiece(Piece piece, Position position) {
        if (position == null) {
            return false;
        }

        int positionX = position.getX();
        int positionY = position.getY();
        if (cells[positionX][positionY] == null) {
            Set<Position> positions = piece.possiblePositions(position, dimension,this);
            if (positions != null) {
            	markAsThreatened(positions);
                cells[positionX][positionY] = piece;
        		
                int index = availablePositions.indexOf(position);
                if(index > -1)
                	availablePositions.remove(index);

                pieces = pieces - 1;
                return true;
            }
        }
        return false;
    }

    public boolean isEmpty(Position position) {
        Piece emptyPiece = cells[position.getX()][position.getY()];
        return emptyPiece == null;
    }
    
    public boolean isEmptyOrThreatened(Position position) {
        Piece emptyPiece = cells[position.getX()][position.getY()];
        return emptyPiece == null || emptyPiece.name == Piece.THREATENED;
    }

    public boolean isValid(Set<ChessBoard> result) {
    	
    	if(pieces == 0)
    		return true;

    	return false;

    	
    }
    
    public boolean isComplete(Set<ChessBoard> result) {
    	
    	if(pieces != 0)
    		return true;

    	int cnt =0;
    	for (ChessBoard board : result) {
    		if(board.equals(this))
    			cnt++;
    	}
    	if(cnt > 0)
    		return false;
    	
    	return true;
    	
    }

    public ChessBoard copy() {
        return new ChessBoard(this);
    }

    private void markAsThreatened(Set<Position> positions) {
        for (Position position : positions) {
        		cells[position.getX()][position.getY()] = new Piece(Piece.THREATENED);
        		int index = availablePositions.indexOf(position);
        		if(index > -1)
        		availablePositions.remove(index);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ChessBoard board = (ChessBoard) o;

        if (this.cells == board.cells) {
            return true;
        }
        
        if ((this.cells == null || board.cells == null) || this.cells.length != board.cells.length) {
            return false;
        }

        for (int x = 0; x < this.cells.length; x++) {
            for (int y = 0; y < this.cells[x].length; y++) {
            	if(this.cells[x][y] == null)
            		this.cells[x][y] = new Piece(Piece.THREATENED);
            	if(board.cells[x][y] == null)
            		board.cells[x][y] = new Piece(Piece.THREATENED);
                if ( this.cells[x][y].name != board.cells[x][y].name) {
                    return false;
                }
            }
        }

        return true;
    }
    
    public static Piece[][] deepCopy(Piece[][] cells2) {
        if (cells2 == null) {
            return null;
        }

        final Piece[][] result = new Piece[cells2.length][];
        for (int i = 0; i < cells2.length; i++) {
            result[i] = Arrays.copyOf(cells2[i], cells2[i].length);
        }
        return result;
    }
    
    @Override
    public String toString() {

        for (int x = 0; x < cells.length; x++) {
            for (int y = 0; y < cells[x].length; y++) {
            	if(cells[x][y] != null)
            		System.out.print(cells[x][y].name);
            	else
            		System.out.print("-");
            }
            System.out.println();
        }
        return "";
    }

}